
Link
=================

.. only:: html or singlehtml

   .. raw:: html

       <div class="a-target-_blank"> <ul><li> <a target="_blank" href="https://sites.google.com/view/sasashima-seminar">愛知大学経済学会ささしまセミナー（通称：ささしまセミナー）  <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></li>愛知大学名古屋キャンパスで開催される愛知大学経済学会主催の経済学のセミナーです．</ul></div>

       <div class="a-target-_blank"> <ul><li><a target="_blank" href="https://www.aichi-u.ac.jp/">Aichi University   <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></li></ul></div>

       <div class="a-target-_blank"> <ul><li><a target="_blank" href="https://www.aichi-u.ac.jp/english/access">Campus Map   <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></li></ul></div>


.. toctree::
   :maxdepth: 1


| 