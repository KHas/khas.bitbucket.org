

Personal Use
~~~~~~~~~~~~~~~~~~~~~~~~~~~

| 
| Personal use, memo




---------------------------------------------------------------------

.. only:: html or singlehtml

   .. raw:: html

       <ul class="simple">
       <li><div class="a-target-_blank"><a target="_blank" href="https://www.overleaf.com/project">Overleaf <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       <li><div class="a-target-_blank"><a target="_blank" href="http://texclip.marutank.net/">TeX Clip <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       </ul>



---------------------------------------------------------------------

.. only:: html or singlehtml

   .. raw:: html

       <ul class="simple">
       <li><div class="a-target-_blank"><a target="_blank" href="https://notebooklm.google/">NotebookLM <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       </ul>


---------------------------------------------------------------------

.. only:: html or singlehtml

   .. raw:: html

       <ul class="simple">
       <li><div class="a-target-_blank"><a target="_blank" href="https://www.notion.so/ja-jp/product">Notion <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       <li><div class="a-target-_blank"><a target="_blank" href="https://typora.io/">typora <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       </ul>


---------------------------------------------------------------------


.. only:: html or singlehtml

   .. raw:: html

       <ul class="simple">
       <li><div class="a-target-_blank"><a target="_blank" href="https://researchmap.jp/khasui">Researchmap <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       <li><div class="a-target-_blank"><a target="_blank" href="https://ideas.repec.org/f/pko606.html">Ideas <img src="https://khas.bitbucket.io/tabicon.png" width="11" height="11"></a></div></li>
       </ul>


---------------------------------------------------------------------


.. toctree::
   :maxdepth: 1

   teaching



| 