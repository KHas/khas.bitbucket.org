.. Kohei Hasui documentation master file, created by
   sphinx-quickstart on Thu Jul 30 15:17:08 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Kohei Hasui
================================

|  Associate Professor
|  Department of Economics, Aichi University, Japan

.. toctree::
   :maxdepth: 1

   profile
   research
   link



|    
