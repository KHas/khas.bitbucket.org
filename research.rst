


Research
=================================


| 
| Last Update: |today|


Publication (main works)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. | "Optimal monetary policy in a liquidity trap: Evaluations for Japan's monetary policy"
   | Kohei Hasui and Yuki Teranishi.
   | *Journal of the Japanese and International Economies* (accepted, Feb 2025)

#. | `"Optimal irreversible monetary policy" <https://doi.org/10.1016/j.euroecorev.2021.103707>`_
   | Kohei Hasui, Teruyoshi Kobayashi,  and Tomohiro Sugo
   | *European Economic Review* 134, 103707, 2021. 

#. | `"A note on robust monetary policy and non-zero trend inflation" <https://doi.org/10.1017/S1365100518000883>`_ 
   | Kohei Hasui 
   | *Macroeconomic Dynamics* 24(6), 1574-1594, 2020.   

#. | `"Role of expectations in a liquidity trap" <https://doi.org/10.1016/j.jjie.2018.12.004>`_ 
   | Kohei Hasui, Yoshiyuki Nakazono, and Yuki Teranishi
   | *Journal of the Japanese and International Economies* 52, 201-215, 2019. 

#. | `"Efficient immunization strategies to prevent financial contagion" <https://doi.org/10.1038/srep03834>`_
   | Teruyoshi Kobayashi and Kohei Hasui
   | *Scientific Reports* 4, 3834, 2014. 

For the complete list of publications, please visit Researchmap.

Working Papers
^^^^^^^^^^^^^^^^^^^^^
* | `"Heterogeneous Inflation Volatility in Production Networks: Producer vs. Consumer Prices" <https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4968158>`_ 
  | K. Hasui and Teruyoshi Kobayashi.
  | SSRN Paper 4968158, Sep, 2024.


Work in progress
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* | `"Liquidity Trap and Optimal Monetary Policy Revisited" <http://www.price.e.u-tokyo.ac.jp/img/researchdata/pdf/p_wp079.pdf>`_  (work in progress, Sep 2023)
  | K. Hasui, Tomohiro Sugo, and Yuki Teranishi
  | UTokyo Price Project Working Paper Series University of Tokyo  No.79, 2016.


Awards
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* | `Research Paper Award <https://www.rieb.kobe-u.ac.jp/edu-society/sympo/workshop_monetary/awards/index.html>`_ (Sep, 2021)
  | Monetary Economics Seminar of Kobe University.
  | (�_�ˑ�w���Z������)


Grants
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Principal Investigator:

* | JSPS KAKENHI 22K01585 Grant-in-Aid for Scientific Research (C).
  | FY2022-2025, JPY2,990,000 (Co-Investigator: `Satoshi Hoshino <https://researchmap.jp/shoshino>`_).
* | The Nitto Foundation. The 38th Research Grant: Number 46.
  | FY2022, JPY 700,000.
* | JSPS KAKENHI 17K13768 Grant-in-Aid for Young Scientists (B).
  | FY2017-2020, JPY3,510,000.

Co-Investigator:

* | Center Project Matsuyama University
  | FY2017-2019 with Tatsuro Kakeshita (Principal Investigator) and Keiichiro Nishio.

* | JSPS KAKENHI 16K03920 Grant-in-Aid for Scientific Research (C).
  | FY2016-2019 (Principal Investigator: Tatsuro Kakeshita).


| 
|