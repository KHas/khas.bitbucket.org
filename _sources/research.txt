Research
========

Publication
^^^^^^^^^^^^^^

3. | �gThe liquidity effect and tightening effect of the zero lower bound�h, Kohei Hasui
   | *Japanese Journal of Monetary and Financial Economics*, 2014, 2(1), 1-15, Japan Society of Monetary Economics

2. | �gEfficient immunization strategies to prevent financial contagion�h, `Teruyoshi Kobayashi <https://terukobayashi.wordpress.com/>`_  and Kohei Hasui,
   | *Scientific Reports* 4, article number: 3834, 2014, Nature Publishing Group

1. | �gFinancial markets and the channels of monetary policy transmission�h, Kohei Hasui and Teruyoshi Kobayashi.
   | *Journal of Economics and Business Administration (Kokumin-Keizai Zasshi)*,
   | 2013, 207(2), 65-78 (in Japanese and non-refreed)


Work in Progress
^^^^^^^^^^^^^^^^^^^^
* | Anticipated shocks and liquidity traps
  | - Presented on October 2016 at JSME Autumn Meeting, 
* Trend inflation, cost channel and monetary policy

* A simple stochastic analysis of negative interest rates in optimal monetary policy

Competitive Grants
^^^^^^^^^^^^^^^^^^^^^^^
* | Co-Investigator
  | Grant-in-Aid for Scientific Research (C), Japan Society for the Promotion of Science (2016 - 2019), (PI: `Tatsuro Kakeshita <http://www.cc.matsuyama-u.ac.jp/~tatsuro/index.html>`_ )

Presentation
^^^^^^^^^^^^^^^

* | 2016:
  | JSME Autumn Meeting (October, Kansai University), Monetary Economics Seminar (October, Matsuyama University), KMSG (March, Kwansei Gakuin University)


* | 2015:
  | The 17th Macroeconomics Conference (November, Hitotsubashi Universuty), SWET (August, Hokkaido University), JSME Autumn Meeting (October, Tohoku Universuty), Japan Association for Applied Economics (May, Kyushu Sangyo University)

* | 2014:
  | JSME Autumn Meeting (October, Yamaguchi University), DSGE Meeting (October, Kobe University), Modern Monetary Economics Summer Institute (October, MME), JEA Spring Meeting (May, Doshisha University),

* | 2013:
  | Japan Society of  Monetary Economics Autumn Meeting (September, Nagoya University), Rokko Forum (November, May, Kobe University)

.. toctree::
   :maxdepth: 1