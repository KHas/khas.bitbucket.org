Short summary of papers
============================


------------------------------------------------------------------------------------------------------

How robustness can change the desirability of speed limit policy
-----------------------------------------------------------------------
| Author(s): Kohei Hasui
| *Scottish Journal of Political Economy*, accepted (Dec 2020).
| `Manuscript <https://www.researchgate.net/publication/346952243_How_robustness_can_change_the_desirability_of_speed_limit_policy>`_ 
| 
| This paper investigates a robust monetary policy under speed limit policy when a central bank fears model misspecification. We show that the persistence of the output gap becomes small with the robust speed limit policy. The low persistence of the output gap contributes to mitigating the variance of the output gap. Also, with a robust policy, social losses are lower under the speed limit policy than under precommitment. Our results suggest that adding the growth of the output gap to the central bank�fs objectives is effective when the worst-case scenario is realized.

| **The model (Walsh, 2003)**
| *Robust control problem:*

.. math::

   &\max_{\nu_t} \min _{i_t} \mathbb{E}_t \sum_{j = 0}^{\infty} \beta ^j (\pi_{t+j}^2 + \lambda (x_{t+j}-x_{t+j-1})^2 + \delta \nu_{t+j}^2)
   \\
   &\text{s.t.}
   \\
   &\pi_t = \beta \mathbb{E}_t\pi_{t+1} + \kappa x_t + (e_t + \nu_t).

| 
| 
| Table 1. Comparison of percentage welfare loss relative to precommitment and welfare equivalent consumption loss.

.. image:: tab_loss_rbpslp.jpg
   :scale: 25%
   :align: center

| 
| 

.. image:: fig_loss_rbpslp.jpg
   :scale: 60%
   :align: center

| Fig. 1. Relative gains to inflation targeting for degrees of :math:`\phi`.
| 
| 


---------------------------------------------------------------------------------------------------


Trend growth and robust monetary policy
-----------------------------------------------------------------------
| Author(s): Kohei Hasui
| *The B.E. Journal of Macroeconomics* (Contributions), forthcoming.
| `Publisher's Page <https://doi.org/10.1515/bejm-2020-0133>`_ 
| 
| Recent monetary policy studies have shown that the trend productivity growth has non-trivial implications for monetary policy. This paper investigates how trend growth alters the effect of model uncertainty on macroeconomic fluctuations by introducing a robust control problem. We show that an increase in trend growth reduces the effect of the central bank's model uncertainty and, hence, mitigates the large macroeconomic fluctuations. Moreover, the increase in trend growth contributes to bringing the economy into determinacy regions even if larger model uncertainty exists. These results indicate that trend growth contributes to stabilizing the economy in terms of both variance and determinacy when model uncertainty exists.
| 
| **The model (Mattesini and Nistico, 2010)**
| *The robust control problem:*

.. math::

   &\min_{\pi_t,x_t,i_t} \max_{\nu_t} \ \mathbb{E}_t\sum_{j = 0}^{\infty} (\beta \gamma^{1-\sigma})^j[L_{t+j} - \theta \nu_{t+j}'\nu_{t+j}]
    \\
    &\text{s.t.}
    \\
    &x_t = \mathbb{E}_tx_{t+1} - \sigma^{-1}_n[i_t - \mathbb{E}_t \pi_{t+1} - (r_t^n + \nu_{t}^r)]
    ,
    \\
    &\pi_t = \beta \gamma^{1-\sigma}\mathbb{E}_t \pi_{t+1} + \kappa(\gamma) \xi x_t + (u_t + \nu_t^u),

| where

.. math::

   L_t = \frac{\epsilon }{\kappa(\gamma)}\pi_t^2 + \xi x_t^2.

| **Proposition 2.** *In the worst-case scenario, a higher trend growth decreases the effect of the central bank's model uncertainty on the inflation rate, the output gap, and the policy rate under Assumptions 1 and 2.*
| 
| *Proof.*  The cross-derivative of :math:`c_{\pi}`, :math:`c_x`, and :math:`c_i` with respect to :math:`\theta` and :math:`\gamma` is given as follows.
    

.. math::

        &-\frac{\partial^2 |c_{\pi}(\theta,\gamma)|}{\partial \theta \partial \gamma}
        =
        -\omega (\theta, \gamma) 
        \frac{\partial \kappa(\gamma)}{\partial \gamma}
        ,
        \label{eq:cross_cpi}
        \\
        &-\frac{\partial^2 |c_x(\theta,\gamma)|}{\partial \theta \partial \gamma}
        =
        -\epsilon\omega (\theta, \gamma) 
        \frac{\partial \kappa(\gamma)}{\partial \gamma}
        ,
        \label{eq:cross_x}
        \\
        &-\frac{\partial^2 |c_i(\theta,\gamma)|}{\partial \theta \partial \gamma}
        =
        -\sigma_n\epsilon\omega (\theta, \gamma) 
        \frac{\partial \kappa(\gamma)}{\partial \gamma}
        ,
        \label{eq:cross_ci}


where


.. math::
        \omega (\theta, \gamma)  = \left[
        \frac{1}{\epsilon}+  \left(\frac{1}{\theta \kappa(\gamma)}+\xi\kappa(\gamma) \right)\frac{2}{\chi(\theta,\gamma)}
        \right] 
        \left(\frac{1}{\theta \kappa(\gamma)\chi(\theta,\gamma)}\right)^2 > 0.

| Whether the signs of Equations are positive or negative depend on the sign of the derivative of the slope with respect to the trend growth: 

.. math::
        \frac{\partial \kappa(\gamma)}{\partial \gamma}
        =
        \frac{\beta (\sigma-1)(1-\alpha)}{\gamma ^{\sigma}} \gtrless 0
        \text{ if }
        \sigma \gtrless 1.
        \label{eq:dkappadgamma}

| Combining these conditions, we determine the signs of the cross-derivative as follows:

.. math::
        &-\frac{\partial^2 |c_{\pi}(\theta,\gamma)|}{\partial \theta \partial \gamma}
        <0,
        \
        -\frac{\partial^2 |c_x(\theta,\gamma)|}{\partial \theta \partial \gamma}
        <0
        ,
        \
        \text{and }
        -\frac{\partial^2 |c_i(\theta,\gamma)|}{\partial \theta \partial \gamma}
        <0
        \text{ if }
        \sigma > 1
        .
        \label{eq:result2a}
        \\
        &-\frac{\partial^2 |c_{\pi}(\theta,\gamma)|}{\partial \theta \partial \gamma}
        >0,
        \
        -\frac{\partial^2 |c_x(\theta,\gamma)|}{\partial \theta \partial \gamma}
        >0
        ,
        \
        \text{and }
        -\frac{\partial^2 |c_i(\theta,\gamma)|}{\partial \theta \partial \gamma}
        >0
        \text{ if }
        \sigma < 1
        .
        \label{eq:result2b}

| Since :math:`\sigma >1`, the signs of inequalities are negative. Therefore, a higher trend growth decreases the effect of the central bank's model uncertainty on the inflation rate, the output gap, and the policy rate. QED.

.. image:: fig_imp_tg.jpg
   :scale: 35%
   :align: center

| Fig. 1. Impulse response to 1% cost-push shock under discretion.

.. image:: fig_determ_tg.jpg
   :scale: 35%
   :align: center

| Fig. 2. Determinate and indeterminate area (:math:`\sigma = 5`, :math:`\gamma = 1.0124`, :math:`\beta= 0.99`, :math:`s_c = 0.8^{-1}`, :math:`\zeta = 13`, :math:`\alpha = 0.7`, :math:`\varphi = 0.5` and :math:`\epsilon = 9.8`)


|
|

-----------------------------------------------------------------------------------------------------------


A note on robust monetary policy and non-zero trend inflation
-----------------------------------------------------------------------
| Author(s): Kohei Hasui
| *Macroeconomic Dynamics* 24(6), 1574-1594, 2020.
| `Publiser's Page <https://doi.org/10.1017/S1365100518000883>`_ 
| 
| This paper studies how model uncertainty influences economic fluctuation when trend inflation is high. We introduce Hansen and Sargent�fs [(2008) *Robustness*, Princeton University Press] robust control techniques into a New Keynesian model with non-zero trend inflation. We reveal the following three points. First, we find that robust monetary policy responds more aggressively. This aggressiveness increases with trend inflation. Second, as the trend inflation rises, the response of macroeconomic variables is larger under robust policy. Third, stronger robustness tends to lead to indeterminate equilibrium as trend inflation increases. Consequently, the economy might be volatile when trend inflation is high due to robustness from the view of both variance and determinacy. We interpret the results as indicating that the model uncertainty might be the one of the factors causing large macroeconomic fluctuations when trend inflation is high.
| 
| **The model (Sbordone, 2007, and Cogley and Sbordone, 2008, Alves, 2012 and 2014)**
| *Robust control problem:*

.. math::


    &\min_{x_t,\pi_t,i_t}\max_{v_{t}}\mathbb{E}_t\sum _{j=0}^{\infty}
    \beta ^j[
    L_{t+j} - \theta v_{t+j}'v_{t+j}]
    \\
    &\text{s.t}
    \\
    &x_t = \mathbb{E}_tx_{t+1} - \sigma [i_t - \mathbb{E}_t\pi_{t+1}-(r_t^n + v^x_t)]
    \\
    &\pi_t
    =
    \mathcal{K}(\Pi)x_t
    +
    \mathcal{B}(\Pi)\mathbb{E}_t\pi_{t+1}
    + h_t
    + \gamma(\Pi)(r_t^n+v_t^x) + (u_t + v^{\pi}_{t})
    \\
    &h_t = \beta \chi (\Pi)\mathbb{E}_tx_{t} + \beta \zeta (\Pi)\mathbb{E}_t\pi_{t+1}
    +
    \phi \mathbb{E}_t h_{t+1} + v_{t}^h,


where


.. math::
    L_t
    =
    \pi_t^2
    +
    \lambda (\Pi)x_t^2,\ \ 
    \lambda (\Pi)
    =
    \frac
    {1-\bar{\alpha}}
    {1-\bar{\alpha}\vartheta}
    \frac{\kappa(\Pi)}
    {\epsilon}.

|
|
| **Proposition 1.** *In the worst-case scenario, Eqs. (11) - (16) show that stronger policy robustness increases sensitivity in inflation, output gap, and policy rate in response to cost-push shock and natural rate shock under Assumption 2.*
|
| **Proposition 2.** *In the worst-case scenario, Eqs. (18) - (21) show that the amount of change in sensitivity in inflation, output gap, and policy rate depend on trend inflation.*

.. image:: fig_stream_trendinf.jpg
   :scale: 40%
   :align: center

| Fig. 1: Percent changes in variances and losses.


| 
| 

-------------------------------------------------------------------------------------------------------------

Role of expectations in a liquidity trap
-----------------------------------------------------------------------
| Author(s): Kohei Hasui, Yoshiyuki Nakazono,  and Yuki Teranishi
| *Journal of the Japanese and International Economies* 52, 201-2015, 2019.
| `Publiser's Page <https://doi.org/10.1016/j.jjie.2018.12.004>`_ 
| 
| A number of previous studies suggest that inflation expectations are important in considering the effectiveness of monetary policy in a liquidity trap. However, the role of inflation expectations can be very different, depending on the type of monetary policy that a central bank implements. This paper reveals how a private agent forms inflation expectation affects the effectiveness of monetary policy under the optimal commitment policy, the Taylor rule, and a simple rule with price-level targeting. We examine two expectation formations: (i) different degrees of anchoring, and (ii) different degrees of forward-lookingness. We show that how to form inflation expectations is less relevant when a central bank implements the optimal commitment policy, while it is critical when the central bank adopts the Taylor rule or a simple rule with price-level targeting. Even for the Japanese economy, the effects of monetary policy on economic dynamics significantly change according to expectation formations under rules other than the optimal commitment policy.

|
| **The model:**


.. math::
    x_t =&\, x_{t+1}^e - \sigma (i_t -\pi_{t+1}^e- r_t^n),
    \\
    \pi_t =&\, \beta \pi_{t+1}^e + \kappa x_t,\label{eq:nkpc}
    \\
    r_t^n =&\, \rho_r r^n_{t-1} + \epsilon_t,\label{eq:ar}
    \\
    i_t \geq &\, 0.

| where

.. math::
    \pi_{t+1}^e := 
    \begin{cases}
    \ \gamma_{\pi} \text{E}_t\pi_{t+1} + (1-\gamma_{\pi})\bar{\pi}, & \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \textrm{(a)}
    \\
    \ \gamma_{\pi} \text{E}_t\pi_{t+1} + (1-\gamma_{\pi})\pi_t, & \ \ \ \ \ \ \ \ \ \ \ \ \ \    \ \  \textrm{(b)}
    \end{cases}

| Optimal commitment policy:

.. math::
    &\pi _{t} -\beta ^{-1}\sigma \gamma_{\pi}\phi _{1t-1} + \phi _{2t}-\gamma_{\pi}\phi _{2t-1}=0,\label{FOC1ae}
    \\
    &\lambda _{x}x_{t}+\phi _{1t}-\beta ^{-1}\phi _{1t-1}-\kappa \phi _{2t}=0,\label{FOC2ae}
    \\
    &i_{t}\phi _{1t}=0,\ \phi _{1t}\geq 0,\  i_{t}\geq 0.\label{KT1ae}

| 
| 

.. image:: fig_impcom_jjie.jpg
   :scale: 20%
   :align: center

| Fig. 3:  Impulse responses to an annual :math:`3\%` natural rate shock under optimal commitment policy for different expectation formations for an inflation rate. Note: the scale of the figure is �esmall�f.


|
|
|