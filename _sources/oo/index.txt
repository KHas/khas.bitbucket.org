.. Kohei Hasui documentation master file, created by
   sphinx-quickstart on Thu Jul 30 15:17:08 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kohei Hasui's Documentation
================================

.. title:: Kohei Hasui



.. figure:: city_view.jpg
   :scale: 47%
   :height: 413px
   :width: 784px
   :align: right
   :alt:

   Matsuyama city, view from Mt. Katsuyama, Mar, 2016


|  @ä N½
|  ¼RåwoÏw ut

.. toctree::
   :maxdepth: 1

   profile
   research
   teaching
   memo

|  *2015-09-10*
|  [~Å×­µ½SphinxÅ{z[y[Wðì¬
|  BitbucketÅöJ