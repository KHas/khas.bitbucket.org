
Profile
=======

Kohei HASUI
-------------
*  `Profile in Japanese <http://khas.bitbucket.org/profilej.html>`_

Office Address
^^^^^^^^^^^^^^^^
| Faculty of Economics, Matsuyama University
| 4-2, Bunkyo cho, Matsuyama, Ehime, 790-8578, Japan

Education
^^^^^^^^^^^^
| Mar 2015, Ph.D. Economics, Kobe University
| Mar 2012, M.A. Economics, Kobe University
| Mar 2010, B.A. Economics, University of Hyogo

Current and Past Positions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| Apr 2016 - Present, Lecturer, Faculty of Economics, Matsuyama University
| Apr 2015 - Mar 2016, Research Fellow, Graduate School of Economics, Kobe University

Research Interests
^^^^^^^^^^^^^^^^^^^^^
| Monetary Policy, Macroeconomics, Computational Economics

Membership
^^^^^^^^^^^^^^
| Japanese Economic Association, Japan Society of Monetary Economics


Referee
^^^^^^^^^
*Japan and the World Economy*

.. toctree::
   :maxdepth: 1
