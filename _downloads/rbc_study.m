% RBC model

clear all
close

% Parameters
betta  = .988; % discount factor
alfa = .4;     % fraction of capital
delta = .025;  % depreciation rate
psy   = 0;     % elasticity of labor
pssy  = 1;     % fraction of disutility
rho   = .95;   % persistence of technology

% calculate steady state
ky = alfa*betta/(1-betta*(1-delta));        % k*/y*
iy = delta*ky;                              % i*/y*
cy = 1-iy;                                  % c*/y*
hstar = ((1-alfa)/(pssy*cy))^(1/(1+psy));   % h*
ystar = ky^(alfa/(1-alfa))*hstar;           % y*
cstar = cy*ystar;                           % c*
istar = iy*ystar;                           % i*
kstar = ky*ystar;                           % k*

% System of the model:
% A*X(t) = B*X(t-1) + C*eps(t) + D*eta(t)
% X = [c, y, h, i, k, Ec, Ey, a]
% eps(t): Technology shock
% eta(t) = [eta_c,eta_y]: Expectation error

% ここから,配布資料を見ながら行列を書いて見てください．

% 
% Eigen value decomposition
% 
% Apply generalized schur(QZ) decomposition for S and T.
%
%  S*E[X(t+1)] = T*X(t)
%   (Q'*V1*Z')*E[X(t+1)] = (Q'*V2*Z')*X(t)
%
%  where QQ' = ZZ' = I(N×N).
%
[S,T,Q,Z] = qz(A,B);
s = diag(S);
t = diag(T);
lambda = abs(t./s) < 1-eps;
%
% lambda is generalized eigenvalue;
%    lambda(i) = t(ii)/s(ii)
%    lambda(i) < 1 is stable root.  
% Then lambda is a dummy vector which return 1 if eigen value is stable. 
%
% reorder by recalling ordqz
[S,T,Q,Z] = ordqz(S,T,Q,Z,lambda);
s = diag(S);
t = diag(T);
lambda = abs(t./s) < 1-eps;
NS = sum(lambda);
%
% NS is the number of state variable.
%

% Blanchard and Kahn condition
if NS == N-neta
    disp(' Checking Blanchard and Kahn condition (BKC)... ')
    disp(['  ',num2str(N-NS),' unstable roots'])
    disp(['  ',num2str(neta),' jump variables'])
    disp(['  ',num2str(NS),' stable roots'])
    disp(['  ',num2str(N-neta),' state variables'])
    disp(' BKC is satisfied.')
    disp(' ')
else
    disp(' Checking Blanchard and Kahn condition (BKC)...')
    disp(['  ',num2str(N-NS),' unstable roots'])
    disp(['  ',num2str(neta),' jump variables'])
    disp(['  ',num2str(NS),' stable roots'])
    disp(['  ',num2str(N-neta),' state variables'])
    error('BKC is not satisfied')
end

% Divide stable and unstable block
%
% Q'*S*Z'*s(t) = Q'*T*Z'*s(t-1)+Psi*eps(t)+Pi*eta(t)
%
% where S and T are upper triangular
%
S11 = S(1:NS,1:NS);
S12 = S(1:NS,NS+1:N);
S22 = S(NS+1:N,NS+1:N);
T11 = T(1:NS,1:NS);
T12 = T(1:NS,NS+1:N);
T22 = T(NS+1:N,NS+1:N);
%
% Solution form is
%
%  s(t) = M*s(t-1)+Me*eps(t),
%
% where
%
%  M = Z*(S11\T11 S11\(T12-Phi*Tss);0 0)*Z'
%  Me = Z*(S11\(Q1-Phi*Q2);0)*C
%  Phi = Q1*D*((Q2*D)\eye(size(Q2*D,2)));
%
Q1 = Q(1:NS,:);   % stable block of Q
Q2 = Q(NS+1:N,:); % unstable block of Q
Phi = Q1*D*((Q2*D)\eye(size(Q2*D,2))); % definition of Phi
GG = zeros(N,N);
GG11 = S11\T11;
GG12 = S11\(T12-Phi*T22);
GG(1:NS,1:NS) = GG11;    % insert GG11 into stable block of GG
GG(1:NS,NS+1:N) = GG12;  % insert GG12 into unstable block of GG
% Mx
Mx = Z*GG*(Z\eye(size(Z,2)));
GZ = zeros(N,N);
GZ(1:NS,:) = S11\(Q1-Phi*Q2);
% Me
Me = Z*GZ*C;

%
% Impulse responses
%

T = 20;    % irf period
X  = zeros(N,T); % box
epsa = 1;

% for initial period
X(:,1) = Me*epsa'; 
% following periods
for t = 2:T
    X(:,t) = Mx*X(:,t-1);
end
%Extradct and set names
c = X(1,:);
y = X(2,:);
n = X(3,:);
i = X(4,:);
k = X(5,:);
Ec = X(6,:);
Ey = X(7,:);
a = X(8,:);


% Plot impulse responses
%
% Figure plot
%
lw = 1;                     % line width of impulse responses
zeroline = zeros(1,T);      % steady state lines

figure(1)

subplot(3,2,1) % output
 h = plot(1:T,y,1:T,zeroline); % plot(irf period, irf, irf period, zeroline)
 set(h(1),'linewidth',lw)      % irf line width
 set(h(2),'color','red')       % zeroline color (set red in this time)
 title('Output (y)')           % irf name
 xlabel('quarters')            % label name of x axis
 ylabel('percent deviation')   % label name of y axis
 
subplot(3,2,2) % consumption
 h = plot(1:T,c,1:T,zeroline);
 set(h(1),'linewidth',lw)
 set(h(2),'color','red')
 title('Consumption (c)')
 
subplot(3,2,3) % investment
 h = plot(1:T,i,1:T,zeroline);
 set(h(1),'linewidth',lw)
 set(h(2),'color','red')
 title('Investment (i)')
 
subplot(3,2,4) % labor
 h = plot(1:T,n,1:T,zeroline);
 set(h(1),'linewidth',lw)
 set(h(2),'color','red')
 title('Labor (h)')

subplot(3,2,5) % technology
 h = plot(1:T,a,1:T,zeroline);
 set(h(1),'linewidth',lw)
 set(h(2),'color','red')
 title('Technology (a)')

